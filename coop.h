#ifdef COOP_IMPLEMENTATION
#undef COOP_IMPLEMENTATION

typedef struct {
    char* key;
    char* value;
} coop_TypePair;

// NOTE: Even though this is a string-to-string map, we are not using stb's string hashmap macros,
// as they act upon non 'static strings. Because all of the strings are going to be literals
// (made via _stringify), it's realloc system is just a waste for us.
coop_TypePair* sub_to_super_types_map = NULL;

#define _coop_stringify(x) #x

typedef struct {
    void* key;
    char* value;
} coop_PtrTypePair;

coop_PtrTypePair* ptrs_to_types_map = NULL;

bool _coop_upcastable(char* type, void* value) {
    (void) value;

    coop_PtrTypePair* ptr = hmgetp_null(ptrs_to_types_map, value);

    if (ptr == NULL)
        return false;

    if (strcmp(type, ptr->value) == 0)
        return true;


    coop_TypePair* current_pair = hmgetp_null(sub_to_super_types_map, ptr->value);

    while (current_pair != NULL) {
        if (strcmp(current_pair->value, type) == 0) {
            return true;
        }
        current_pair = hmgetp_null(sub_to_super_types_map, current_pair->value);
    }

    return false;
}

#define coop_upcastable(type, value) _coop_upcastable(_coop_stringify(type), value)

bool _coop_isinstance(char* target_type, void* ptr) {
    coop_PtrTypePair* pair = hmgetp_null(ptrs_to_types_map, ptr);

    if (pair == NULL)
        return false;

    return _coop_upcastable(target_type, ptr);
}

#define coop_isinstance(type, value) _coop_isinstance(_coop_stringify(type), value)

#define coop_downcast(type, value) (coop_isinstance(type, value) ? (type*) value : _coop_err_cant_cast_and_exit(_coop_stringify(type), __LINE__, __FILE_NAME__, false))

void* _coop_prepare_memory_for_new(size_t size, char* type_name, bool zero) {
    void* mem = malloc(size);

    if (zero)
        memset(mem, 0, size);

    hmput(ptrs_to_types_map, mem, type_name);

    return mem;
}

#define coop_new(type, value) (*((type*) _coop_prepare_memory_for_new(sizeof(type), _coop_stringify(type), false)) = (value))

#define coop_new_zeroed(type) (_coop_prepare_memory_for_new(sizeof(type), _coop_stringify(type), true))

_Noreturn void* _coop_err_cant_cast_and_exit(char* to, size_t line, char* fname, bool upcasting) {
    fprintf(stderr, "%s:%zu: ERROR: Could not %s to %s! Aborting!\n",
                    fname,
                    line,
                    upcasting ? "upcast" : "downcast",
                    to);
    exit(1);
}

#define coop_upcast(to, item) (coop_upcastable(to, item) \
                                ? (to*) item                                       \
                                : _coop_err_cant_cast_and_exit(_coop_stringify(to), __LINE__, __FILE_NAME__, true))

#define coop_register_subclass(superclass, subclass)                                              \
    do {                                                                                     \
        if ((offsetof(subclass, super) != 0)) {                                              \
            fprintf(stderr,                                                                  \
                    "Error registering subclass %s: %s.super is not the first attribute!\n", \
                    _coop_stringify(subclass),                                                    \
                    _coop_stringify(subclass));                                                   \
            exit(1);                                                                         \
        }                                                                                    \
        subclass s = {0};                                                                    \
        /* TODO: errors in register_subclass if s.super and superclass are the same size     \
        they do not have to be of the same type, which can lead to errors. */                \
        if (sizeof(s.super) != sizeof(superclass)) {                                         \
                fprintf(stderr,                                                              \
                        "Error registering subclass: %s.super is not of type %s!\n",         \
                        _coop_stringify(subclass),                                                \
                        _coop_stringify(superclass));                                             \
                exit(1);                                                                     \
        }                                                                                    \
        (void) s;                                                                            \
        hmput(sub_to_super_types_map, _coop_stringify(subclass), _coop_stringify(superclass));         \
    } while (0);

int coop_delete(void* item) {
    return hmdel(ptrs_to_types_map, item);
}

#ifndef COOP_PREFIXES
#define new_zeroed coop_new_zeroed
#define new coop_new
#define register_subclass coop_register_subclass
#define upcast coop_upcast
#define upcastable coop_upcastable
#define isinstance coop_isinstance
#define downcast coop_downcast
#define delete coop_delete
#endif // COOP_PREFIXES

#endif // COOP_IMPLEMENTATION
