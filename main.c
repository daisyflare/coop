#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

#define COOP_IMPLEMENTATION
#include "coop.h"

typedef struct {
    int a;
} Super;

typedef struct {
    Super super;
    int b, c;
} SubInt;

typedef struct {
    SubInt super;
    int d;
} SubSubInt;

typedef struct {
    Super super;
    char dat[14];
} SubChars;

int main(void) {
    register_subclass(Super, SubInt);
    register_subclass(SubInt, SubSubInt);
    register_subclass(Super, SubChars);

#if 0
    SubSubInt* s = new_zeroed(SubSubInt);
    *s = (SubSubInt) { .super = (SubInt) { .super = (Super) { .a = 5}, .b = 4, .c = 3 }, .d = 2 };
#else
    SubChars* s = new_zeroed(SubChars);
    *s = (SubChars) { .super = (Super) { .a = 6 }, .dat = { 'h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!', '\n' } };
#endif

    Super* sup = upcast(Super, s);
    printf("a = %d!\n", sup->a);
    sup->a = 1;
    if (isinstance(SubChars, sup)) {
        SubChars* downcasted = downcast(SubChars, sup);
        printf("%.*s", (int) sizeof(downcasted->dat), downcasted->dat);
    } else if (isinstance(SubSubInt, sup)) {
        SubSubInt* downcasted = downcast(SubSubInt, sup);
        printf("a = %d!\n", downcasted->super.super.a);
    }
    delete(sup);
    return 0;
}
